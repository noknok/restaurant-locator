module.exports = function(app, db, sql, bParser) {
    var jsonParser = bParser.json()

    //sp_select_dailyandmonthlysales

    app.get('/getsalesbyrestaurantdailyandmonthlysales/:id/:start/:end', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
          request.input('RestaurantId', sql.Int, req.params.id)
          request.input('StartDate', sql.VarChar(10), req.params.start)
          request.input('EndDate', sql.VarChar(10), req.params.end)
          request.execute('sp_select_dailyandmonthlysales', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })

    // app.get('/getsalesbyrestaurantdailysales/:id/:start/:end', (req, res) => {
    //   sql.connect(db, function() {
    //       var request = new sql.Request();
    //       request.input('RestaurantId', sql.Int, req.params.id)
    //       request.input('StartDate', sql.VarChar(10), req.params.start)
    //       request.input('EndDate', sql.VarChar(10), req.params.end)
    //       request.execute('sp_select_dailysales', function(err, recordset) {
    //           if(err) console.log(err)
    //           res.end(JSON.stringify(recordset))
    //           sql.close()
    //         })
    //   })
    // })

    // app.get('/getsalesbyrestaurantmonthlysales/:name/:start/:end', (req, res) => {
    //   sql.connect(db, function() {
    //       var request = new sql.Request();
    //       request.input('RestaurantId', sql.Int, req.params.id)
    //       request.input('StartDate', sql.VarChar(10), req.params.start)
    //       request.input('EndDate', sql.VarChar(10), req.params.end)
    //       request.execute('sp_select_monthlysales', function(err, recordset) {
    //           if(err) console.log(err)
    //           res.end(JSON.stringify(recordset))
    //           sql.close()
    //         })
    //   })
    // })

    app.get('/getsalesbyrestaurantandrangedate/:name/:start/:end', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
          console.log('RestaurantName: ', req.params.name)
          console.log('StartDate: ', req.params.start)
          console.log('EndDate: ', req.params.end)

          request.input('RestaurantName', sql.VarChar(200), req.params.name)
          request.input('StartDate', sql.VarChar(10), req.params.start)
          request.input('EndDate', sql.VarChar(10), req.params.end)
          request.query('SELECT * FROM view_sales WHERE RestaurantName = @RestaurantName AND (OrderDate >= dbo.func_FormatDateOnly(@StartDate) AND OrderDate <= dbo.func_FormatDateOnly(@EndDate))', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })

    app.get('/getsalesbyid/:id', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.input('SalesId', sql.Int, req.params.id)
            request.query('SELECT * FROM view_sales WHERE SalesId = @SalesId', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })

    app.get('/getsales', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.query('SELECT * FROM view_sales', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })

   
  
    app.post('/addsales', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('ItemOrderId', sql.Int, req.body.ItemOrderId)
        request.input('CustomerId', sql.Int, req.body.CustomerId)
        request.execute('sp_insert_sales', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
   
    app.put('/modifysales', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('SalesId', sql.Int, req.body.SalesId)
        request.input('ItemOrderId', sql.Int, req.body.ItemOrderId)
        request.input('CustomerId', sql.Int, req.body.CustomerId)
        request.execute('sp_update_sales', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
    app.delete('/deletesales/:id', function(req, res) {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('SalesId', sql.Int, req.params.id)
          request.execute('sp_delete_sales', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
  
  }