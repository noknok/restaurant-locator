
module.exports = function(app, db, sql, bParser) {
    var jsonParser = bParser.json()

    // app.get('/getrestaurantbyid/:id', (req, res) => {
    // // request.input('RestaurantId', sql.Int, req.params.id);

    app.get('/getrestaurantbyid/:id', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.input('RestaurantId', sql.Int, req.params.id);
            request.query('SELECT * FROM view_restaurant WHERE RestaurantId = @RestaurantId', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })

    app.get('/getrestaurant', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.query('SELECT * FROM view_restaurant', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })
  
    app.post('/addrestaurant', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('Name', sql.VarChar(200), req.body.Name)
        request.input('Address', sql.VarChar(400), req.body.Address)
        request.input('RestaurantSpecialtyId', sql.Int, req.body.RestaurantSpecialtyId)
        request.input('RestaurantCategoryId', sql.Int, req.body.RestaurantCategoryId)
        request.input('Longitude', sql.NVarChar(500), req.body.Longitude)
        request.input('Latitude', sql.NVarChar(500), req.body.Latitude)
        request.execute('sp_insert_restaurant', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
   
    app.put('/modifyrestaurant', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('RestaurantId', sql.Int, req.body.RestaurantId)
        request.input('Name', sql.VarChar(200), req.body.Name)
        request.input('Address', sql.VarChar(400), req.body.Address)
        request.input('RestaurantSpecialtyId', sql.Int, req.body.RestaurantSpecialtyId)
        request.input('RestaurantCategoryId', sql.Int, req.body.RestaurantCategoryId)
        request.input('Longitude', sql.NVarChar(500), req.body.Longitude)
        request.input('Latitude', sql.NVarChar(500), req.body.Latitude)
        request.execute('sp_update_restaurant', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
    app.delete('/deleterestaurant/:id', function(req, res) {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('RestaurantId', sql.Int, req.params.id)
          request.execute('sp_delete_restaurant', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
  
  }