module.exports = function(app, db, sql, bParser) {
  
  // var urlencodedParser = bParser.urlencoded({ extended: false })
  var jsonParser = bParser.json()

  app.get('/getcustomer', (req, res) => {
    sql.connect(db, function() {
        var request = new sql.Request();
          request.query('SELECT * FROM Customer', function(err, recordset) {
            if(err) console.log(err)
            res.end(JSON.stringify(recordset))
            sql.close()
          })
    })
  })

  app.post('/addcustomer', jsonParser, (req, res) => {
    sql.connect(db, function() {
      var request = new sql.Request()
       request.input('FirstName', sql.VarChar(100), req.body.FirstName)
       request.input('MiddleName', sql.VarChar(100), req.body.MiddleName)
       request.input('LastName', sql.VarChar(100),  req.body.LastName)
       request.input('CustomerAddress', sql.VarChar(400),  req.body.CustomerAddress)
       request.input('ContactNo', sql.VarChar(15),  req.body.ContactNo)
      // .output('output_parameter', sql.VarChar(50))
      .execute('sp_insert_customer', function(err, recordset) {
        if(err) console.log(err)
        res.end(JSON.stringify(recordset))
        sql.close()
      })
    })
  })
 // {"CustomerId": 1, "FirstName":"My Name"}
  app.put('/modifycustomer', jsonParser, (req, res) => {
    sql.connect(db, function() {
      var request = new sql.Request()
      request.input('CustomerId', sql.Int, req.body.CustomerId)
      request.input('FirstName', sql.VarChar(100), req.body.FirstName)
      request.input('MiddleName', sql.VarChar(100), req.body.MiddleName)
      request.input('LastName', sql.VarChar(100),  req.body.LastName)
      request.input('CustomerAddress', sql.VarChar(400),  req.body.CustomerAddress)
      request.input('ContactNo', sql.VarChar(15),  req.body.ContactNo)
      // .output('output_parameter', sql.VarChar(50))
      request.execute('sp_update_customer', function(err, recordset) {
        if(err) console.log(err)
        res.end(JSON.stringify(recordset))
        sql.close()
      })
    })
  })

  //http://localhost:8081/deletecustomer/18
  app.delete('/deletecustomer/:id', function(req, res) {
    sql.connect(db, function() {
      var request = new sql.Request()
        request.input('CustomerId', sql.Int, req.params.id)
        request.execute('sp_delete_customer', function(err, recordset) {
        if(err) console.log(err)
        res.end(JSON.stringify(recordset))
        sql.close()
      })
    })
  })


}