module.exports = function(app, db, sql, bParser) {
    var jsonParser = bParser.json()

    app.get('/getitem', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.query('SELECT * FROM view_item', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })
  
    app.post('/additem', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('ItemName', sql.VarChar(200), req.body.ItemName)
        request.input('Description', sql.VarChar(500), req.body.Description)
        request.input('RestaurantId', sql.Int, req.body.RestaurantId)
        request.input('Price', sql.Float, req.body.Price)
        request.execute('sp_insert_item', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
   
    app.put('/modifyitem', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('ItemId', sql.Int, req.body.ItemId)
        request.input('ItemName', sql.VarChar(200), req.body.ItemName)
        request.input('Description', sql.VarChar(500), req.body.Description)
        request.input('RestaurantId', sql.Int, req.body.RestaurantId)
        request.input('Price', sql.Float, req.body.Price)
        request.execute('sp_update_item', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
    app.delete('/deleteitem/:id', function(req, res) {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('ItemId', sql.Int, req.params.id)
          request.execute('sp_delete_item', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
  
  }