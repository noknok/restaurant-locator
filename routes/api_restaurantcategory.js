module.exports = function(app, db, sql, bParser) {
    var jsonParser = bParser.json()

    app.get('/getrestaurantcategory', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.query('SELECT * FROM RestaurantCategory', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })
  
    app.post('/addrestaurantcategory', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('Category', sql.VarChar(200), req.body.Category)
          request.execute('sp_insert_restaurantcategory', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
   
    app.put('/modifyrestaurantcategory', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('RestaurantCategoryId', sql.Int, req.body.RestaurantCategoryId)
          request.input('Category', sql.VarChar(200), req.body.Category)
          request.execute('sp_update_restaurantcategory', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
    app.delete('/deleterestaurantcategory/:id', function(req, res) {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('RestaurantCategoryId', sql.Int, req.params.id)
          request.execute('sp_delete_restaurantcategory', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
  
  }