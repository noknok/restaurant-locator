module.exports = function(app, db, sql, bParser) {
    var jsonParser = bParser.json()

    app.get('/getitemorder', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.query('SELECT * FROM view_itemorder', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })
  
    app.post('/additemorder', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('ItemOrderId', sql.Int, req.body.ItemOrderId)
        request.input('ItemId', sql.Int, req.body.ItemId)
        request.input('Quantity', sql.Float, req.body.Quantity)
        request.execute('sp_insert_itemorder', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
   
    app.put('/modifyitemorder', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('ItemOrderUIniqueId', sql.Int, req.body.ItemOrderUIniqueId)
        request.input('ItemOrderId', sql.Int, req.body.ItemOrderId)
        request.input('ItemId', sql.Int, req.body.ItemId)
        request.input('Quantity', sql.Float, req.body.Quantity)
        request.execute('sp_update_itemorder', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
    app.delete('/deleteitemorder/:id', function(req, res) {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('ItemOrderUIniqueId', sql.Int, req.params.id)
          request.execute('sp_delete_itemorder', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
  
  }