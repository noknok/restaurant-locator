module.exports = function(app, db, sql, bParser) {
    var jsonParser = bParser.json()

    app.get('/getrestaurantspecialty', (req, res) => {
      sql.connect(db, function() {
          var request = new sql.Request();
            request.query('SELECT * FROM RestaurantSpecialty', function(err, recordset) {
              if(err) console.log(err)
              res.end(JSON.stringify(recordset))
              sql.close()
            })
      })
    })
  
    app.post('/addrestaurantspecialty', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('Specialty', sql.VarChar(200), req.body.Specialty)
        request.execute('sp_insert_restaurantspecialty', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
   
    app.put('/modifyrestaurantspecialty', jsonParser, (req, res) => {
      sql.connect(db, function() {
        var request = new sql.Request()
        request.input('RestaurantSpecialtyId', sql.Int, req.body.RestaurantSpecialtyId)
        request.input('Specialty', sql.VarChar(200), req.body.Specialty)
        request.execute('sp_update_restaurantspecialty', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
    app.delete('/deleterestaurantspecialty/:id', function(req, res) {
      sql.connect(db, function() {
        var request = new sql.Request()
          request.input('RestaurantSpecialtyId', sql.Int, req.params.id)
          request.execute('sp_delete_restaurantspecialty', function(err, recordset) {
          if(err) console.log(err)
          res.end(JSON.stringify(recordset))
          sql.close()
        })
      })
    })
  
  
  }