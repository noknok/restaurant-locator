var express = require("express")
var bodyParser = require("body-parser")
var sql = require("mssql")
var app = express();

var api_restaurantcategory = require('./routes/api_restaurantcategory');
var api_restaurantspecialty = require('./routes/api_restaurantspecialty');
var api_restaurant = require('./routes/api_restaurant');
var api_item = require('./routes/api_item');
var api_itemorder = require('./routes/api_itemorder');
var api_customer = require('./routes/api_customer');
var api_sales = require('./routes/api_sales');


app.use(bodyParser.json())
app.use(bodyParser.json({ type: 'application/*+json' })) 
// app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS, POST, PUT")
    res.header("Access-Control-Allow-Headers", "Origin, X=Request-With, contentType, Accept, Authorization")
    next()
})

var config = {
    user: 'sa',
    password: '123456', // password: 'P@ssw0rd',
    server: 'DESKTOP-7RSKA67',
    database: 'Restaurant'
}

api_restaurantcategory(app, config, sql, bodyParser);
api_restaurantspecialty(app, config, sql, bodyParser);
api_restaurant(app, config, sql, bodyParser);
api_item(app, config, sql, bodyParser);
api_itemorder(app, config, sql, bodyParser);
api_customer(app, config, sql, bodyParser);
api_sales(app, config, sql, bodyParser);

var server = app.listen(8081, function() {
    var host =  server.address().address
    var port =  server.address().port
    console.log("app is listening to http://%s:%s", host, port)
});
